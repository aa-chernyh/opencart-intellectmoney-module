<?php

require_once("IntellectMoneyCommon/LanguageHelper.php");
require_once("IntellectMoneyCommon/UserSettings.php");
require_once("IntellectMoneyCommon/VATs.php");

class ModelIntellectMoneyIntellectmoneyModel extends Model {

    public function getLanguageHelperInstance($lang = 'ru', $isCp1251 = NULL) {
        return \PaySystem\LanguageHelper::getInstance($lang, $isCp1251);
    }

    public function getUserSettingsInstance($params = array(), $isAllowChangeValues = true) {
        return \PaySystem\UserSettings::getInstance($params, $isAllowChangeValues);
    }

    public function getVATsList() {
        return \PaySystem\VATs::getList();
    }

}
