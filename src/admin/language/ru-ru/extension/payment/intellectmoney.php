<?php

$_['code'] = 'ru';
$_['heading_title'] = 'IntellectMoney';
$_['text_edit'] = 'Изменить модуль оплаты IntellectMoney';

$_['text_payment'] = 'Оплата';
$_['text_success'] = 'Настройки модуля обновлены!';
$_['text_intellectmoney'] = '<a onclick="window.open(\'https://intellectmoney.ru/\');"><img src="/admin/view/image/payment/intellectmoney.png" alt="IntellectMoney" title="IntellectMoney" style="width: 96px;border: 1px solid #EEEEEE;" /></a>';
$_['text_enabled'] = "Включен";
$_['text_disabled'] = "Выключен";
$_['text_all_zones'] = "Все зоны";

$_['entry_total'] = 'Сумма';
$_['entry_geo_zone'] = 'Географическая зона';
$_['entry_status'] = 'Статус';
$_['entry_sort_order'] = 'Порядок сортировки';

$_['error_permission'] = 'У Вас нет прав для управления этим модулем!';
?>