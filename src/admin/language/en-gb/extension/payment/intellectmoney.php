<?php

$_['heading_title'] = 'IntellectMoney';
$_['text_edit'] = 'Change payment module IntellectMoney';

$_['text_payment'] = 'Payment';
$_['text_success'] = 'Module settings have been updated!';
$_['text_intellectmoney'] = '<a onclick="window.open(\'https://intellectmoney.ru/\');"><img src="/admin/view/image/payment/intellectmoney.png" alt="IntellectMoney" title="IntellectMoney" style="width: 96px;border: 1px solid #EEEEEE;" /></a>';

$_['entry_total'] = 'Total';
$_['entry_geo_zone'] = 'Geographical area';
$_['entry_status'] = 'Status';
$_['entry_sort_order'] = 'Sorting order';

$_['error_permission'] = 'You are not allowed to control this module!';
?>