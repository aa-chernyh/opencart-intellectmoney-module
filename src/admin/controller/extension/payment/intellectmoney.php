<?php

class ControllerExtensionPaymentIntellectmoney extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('extension/payment/intellectmoney');

        if (method_exists($this->document, 'setTitle'))
            $this->document->setTitle($this->language->get('heading_title'));
        else
            $this->document->title = $this->language->get('heading_title');

        $this->load->model('setting/setting');
        $this->load->model('IntellectMoney/intellectmoney_model');
        $im_lang = $this->model_IntellectMoney_intellectmoney_model->getLanguageHelperInstance($this->language->data['code']);
        $im_userSettings = $this->model_IntellectMoney_intellectmoney_model->getUserSettingsInstance();

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate($im_lang, $im_userSettings))) {
            foreach ($this->request->post as &$value) {
                $value = htmlspecialchars_decode($value);
            }

            $this->model_setting_setting->editSetting('payment_intellectmoney', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true));
        }

        //map breadcrums
        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_payment'),
            'href' => $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/payment/intellectmoney', 'user_token=' . $this->session->data['user_token'], true)
        );
        $data['action'] = $this->url->link('extension/payment/intellectmoney', 'user_token=' . $this->session->data['user_token'], true);
        $data['cancel'] = $this->url->link('marketplace/extension', 'user_token=' . $this->session->data['user_token'] . '&type=payment', true);

        //map Titles
        foreach ($im_userSettings->getNamesOfOrganizationParamsToSave() as $value) {
            $data['entry_' . $value] = $im_lang->getTitle($value);
        }
        $data['entry_total'] = $this->language->get('entry_total');
        $data['entry_resultUrl'] = $im_lang->getTitle('resultUrl');
        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['tab_general'] = $this->language->get('tab_general');
        $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_all_zones'] = $this->language->get('text_all_zones');

        //map Errors
        $data['error_warning'] = isset($this->error['warning']) ? $this->error['warning'] : "";
        foreach ($this->error as $key => $value) {
            $data['error_' . $key] = $value;
        }

        //map Values
        foreach ($im_userSettings->getNamesOfOrganizationParamsToSave() as $value) {
            if (isset($this->request->post['payment_intellectmoney_' . $value])) {
                $funcName = 'get' . ucfirst($value);
                $data['payment_intellectmoney_' . $value] = $this->request->post['payment_intellectmoney_' . $value];
            } else {
                $data['payment_intellectmoney_' . $value] = $this->config->get('payment_intellectmoney_' . $value);
            }
        }
        $this->load->model('localisation/geo_zone');
        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
        $data['payment_intellectmoney_total'] = isset($this->request->post['payment_intellectmoney_total']) ? $this->request->post['payment_intellectmoney_total'] : $this->config->get('payment_intellectmoney_total');
        $data['payment_intellectmoney_geo_zone_id'] = isset($this->request->post['payment_intellectmoney_geo_zone_id']) ? $this->request->post['payment_intellectmoney_geo_zone_id'] : $this->config->get('payment_intellectmoney_geo_zone_id');
        $data['payment_intellectmoney_status'] = isset($this->request->post['payment_intellectmoney_status']) ? $this->request->post['payment_intellectmoney_status'] : $this->config->get('payment_intellectmoney_status');
        $data['payment_intellectmoney_sort_order'] = isset($this->request->post['payment_intellectmoney_sort_order']) ? $this->request->post['payment_intellectmoney_sort_order'] : $this->config->get('payment_intellectmoney_sort_order');
        $data['payment_intellectmoney_total'] = isset($this->request->post['payment_intellectmoney_total']) ? $this->request->post['payment_intellectmoney_total'] : $this->config->get('payment_intellectmoney_total');
        $data['payment_intellectmoney_geo_zone_id'] = isset($this->request->post['payment_intellectmoney_geo_zone_id']) ? $this->request->post['payment_intellectmoney_geo_zone_id'] : $this->config->get('payment_intellectmoney_geo_zone_id');
        $data['payment_intellectmoney_status'] = isset($this->request->post['payment_intellectmoney_status']) ? $this->request->post['payment_intellectmoney_status'] : $this->config->get('payment_intellectmoney_status');
        $data['payment_intellectmoney_sort_order'] = isset($this->request->post['payment_intellectmoney_sort_order']) ? $this->request->post['payment_intellectmoney_sort_order'] : $this->config->get('payment_intellectmoney_sort_order');
        $data['payment_intellectmoney_resultUrl'] = HTTP_CATALOG . 'index.php?route=extension/payment/intellectmoney/callback';

        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();
        $data['VATs'] = $this->model_IntellectMoney_intellectmoney_model->getVATsList();
        $data['statusesIm'] = array(
            array(
                'name' => $data['entry_statusCreated'],
                'dataName' => 'payment_intellectmoney_statusCreated',
                'value' => $data['payment_intellectmoney_statusCreated']
            ),
            array(
                'name' => $data['entry_statusCancelled'],
                'dataName' => 'payment_intellectmoney_statusCancelled',
                'value' => $data['payment_intellectmoney_statusCancelled']
            ),
            array(
                'name' => $data['entry_statusPaid'],
                'dataName' => 'payment_intellectmoney_statusPaid',
                'value' => $data['payment_intellectmoney_statusPaid']
            ),
            array(
                'name' => $data['entry_statusHolded'],
                'dataName' => 'payment_intellectmoney_statusHolded',
                'value' => $data['payment_intellectmoney_statusHolded']
            ),
            array(
                'name' => $data['entry_statusPartiallyPaid'],
                'dataName' => 'payment_intellectmoney_statusPartiallyPaid',
                'value' => $data['payment_intellectmoney_statusPartiallyPaid']
            ),
            array(
                'name' => $data['entry_statusRefunded'],
                'dataName' => 'payment_intellectmoney_statusRefunded',
                'value' => $data['payment_intellectmoney_statusRefunded']
            ),
        );

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        $this->response->setOutput($this->load->view('extension/payment/intellectmoney', $data));
    }

    private function validate($im_lang, $im_userSettings) {
        if (!$this->user->hasPermission('modify', 'extension/payment/intellectmoney')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        foreach ($im_userSettings->getNamesOfOrganizationParamsToSave() as $paramName) {
            $methodName = 'set' . ucfirst($paramName);
            $paramValue = isset($this->request->post['payment_intellectmoney_' . $paramName]) ? $this->request->post['payment_intellectmoney_' . $paramName] : "";
            if (in_array($paramName, array('holdMode', 'testMode')) && $paramValue == 'on') {
                $paramValue = 1;
            }
            if (!$im_userSettings->$methodName($paramValue)) {
                $this->error[$paramName] = $im_lang->getDesc($paramName);
            }
        }
        return empty($this->error);
    }

}

?>