<?php

class ControllerExtensionPaymentIntellectmoney extends Controller {

    public function index() {
        $im_userSettings = $this->loadSettings();
        $orderInfo = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        $im_customer = $this->model_extension_payment_intellectmoney->getCustomerInstance($orderInfo['email'], $orderInfo['shipping_firstname'] . " " . $orderInfo['shipping_lastname'], $orderInfo['telephone']);
        $im_order = $this->model_extension_payment_intellectmoney->getOrderInstance(NULL, $orderInfo['order_id'], $orderInfo['total'], $orderInfo['total'], NULL, NULL, $orderInfo['currency_code'], NULL, NULL);
        //products
        foreach ($this->cart->getProducts() as $product) {
            $im_order->addItem($product['price'], $product['quantity'], $product['name'], $im_userSettings->getTax());
        }
        //delivery
        if (isset($this->session->data['shipping_methods'])) {
            $im_order->addItem($this->session->data['shipping_methods']['flat']['quote']['flat']['cost'], 1, $this->session->data['shipping_methods']['flat']['quote']['flat']['title'], $im_userSettings->getDeliveryTax());
        }

        $im_payment = $this->model_extension_payment_intellectmoney->getPaymentInstance($im_userSettings, $im_order, $im_customer, $this->language->data['code']);
        $data['form'] = $im_payment->generateForm(true);
        $data['button_confirm'] = $this->language->get('button_confirm');

        return $this->load->view('extension/payment/intellectmoney', $data);
    }

    public function callback() {
        $im_userSettings = $this->loadSettings();
        $request = isset($this->request->get['orderId']) ? $this->request->get : $this->request->post;
        $orderInfo = $this->model_checkout_order->getOrder($request['orderId']);
        $im_order = $this->model_extension_payment_intellectmoney->getOrderInstance($this->model_extension_payment_intellectmoney->getinvoiceIdFromOrder($orderInfo['order_id']), $orderInfo['order_id'], $orderInfo['total'], $orderInfo['total'], NULL, NULL, $orderInfo['currency_code'], NULL, $this->model_extension_payment_intellectmoney->getImStatusFromOrder($orderInfo['order_id']));
        $im_result = $this->model_extension_payment_intellectmoney->getResultInstance($request, $im_userSettings, $im_order, $this->language->data['code']);

        $response = $im_result->processingResponse();
        if ($response->changeStatusResult) {
            if (!$this->model_extension_payment_intellectmoney->getinvoiceIdFromOrder($request['orderId'])) {
                $this->model_extension_payment_intellectmoney->saveInvoiceIdToOrder($request['orderId'], $request['paymentId']);
            }
            $this->model_extension_payment_intellectmoney->saveImStatusToOrder($request['orderId'], $request['paymentStatus']);
            $this->model_extension_payment_intellectmoney->changeOrderStatusCMS($request['orderId'], $response->statusCMS);
        }

        echo $im_result->getMessage();
        die;
    }

    private function loadSettings() {
        $this->load->language('extension/payment/intellectmoney');
        $this->load->model('extension/payment/intellectmoney');
        $this->load->model('checkout/order');

        $im_userSettings = $this->model_extension_payment_intellectmoney->getUserSettingsInstance();
        $params = array();
        foreach ($im_userSettings->getNamesOfOrganizationParamsToSave() as $value) {
            if (in_array($value, array('holdMode', 'testMode')) && $this->config->get('payment_intellectmoney_' . $value) == 'on') {
                $params[$value] = 1;
            } else {
                $params[$value] = $this->config->get('payment_intellectmoney_' . $value);
            }
        }
        $im_userSettings->setParams($params);
        $im_userSettings->setMerchantUrl('https://merchant.intellectmoney.ru');
        return $im_userSettings;
    }

}

?>