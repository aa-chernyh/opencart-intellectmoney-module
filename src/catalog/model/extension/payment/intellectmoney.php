<?php

require_once("admin/model/IntellectMoney/IntellectMoneyCommon/LanguageHelper.php");
require_once("admin/model/IntellectMoney/IntellectMoneyCommon/UserSettings.php");
require_once("admin/model/IntellectMoney/IntellectMoneyCommon/Order.php");
require_once("admin/model/IntellectMoney/IntellectMoneyCommon/Customer.php");
require_once("admin/model/IntellectMoney/IntellectMoneyCommon/Payment.php");
require_once("admin/model/IntellectMoney/IntellectMoneyCommon/Result.php");

class ModelExtensionPaymentIntellectMoney extends Model {

    public function getMethod($address, $total) {
        $this->load->language('extension/payment/intellectmoney');

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "zone_to_geo_zone WHERE geo_zone_id = '" . (int) $this->config->get('payment_intellectmoney_geo_zone_id') . "' AND country_id = '" . (int) $address['country_id'] . "' AND (zone_id = '" . (int) $address['zone_id'] . "' OR zone_id = '0')");

        if ($this->config->get('payment_intellectmoney_total') > 0 && $this->config->get('payment_intellectmoney_total') > $total) {
            $status = false;
        } elseif (!$this->config->get('payment_intellectmoney_geo_zone_id')) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }

        $method_data = array();
        if ($status) {
            $method_data = array(
                'code' => 'intellectmoney',
                'title' => $this->language->get('text_title'),
                'terms' => '',
                'sort_order' => $this->config->get('payment_intellectmoney_sort_order')
            );
        }

        return $method_data;
    }

    public function getLanguageHelperInstance($lang = 'ru', $isCp1251 = NULL) {
        return \PaySystem\LanguageHelper::getInstance($lang, $isCp1251);
    }

    public function getUserSettingsInstance($params = array(), $isAllowChangeValues = true) {
        return \PaySystem\UserSettings::getInstance($params, $isAllowChangeValues);
    }

    public function getOrderInstance($invoiceId = NULL, $orderId = NULL, $originalAmount = NULL, $recipientAmount = NULL, $paidAmount = NULL, $deliveryAmount = NULL, $recipientCurrency = NULL, $discount = NULL, $status = NULL) {
        return \PaySystem\Order::getInstance($invoiceId, $orderId, $originalAmount, $recipientAmount, $paidAmount, $deliveryAmount, $recipientCurrency, $discount, $status);
    }

    public function getCustomerInstance($email = NULL, $name = NULL, $phone = NULL) {
        return \PaySystem\Customer::getInstance($email, $name, $phone);
    }

    public function getPaymentInstance($UserSettings = NULL, $Order = NULL, $Customer = NULL, $lang = 'ru') {
        return \PaySystem\Payment::getInstance($UserSettings, $Order, $Customer, $lang);
    }

    public function getResultInstance($request = array(), $UserSettings = "", $Order = "", $lang = 'ru', $isCp1251 = false) {
        return \PaySystem\Result::getInstance($request, $UserSettings, $Order, $lang, $isCp1251);
    }

    public function changeOrderStatusCMS($orderId, $statusCMS) {
        $this->load->model('checkout/order');
        $this->model_checkout_order->addOrderHistory($orderId, $statusCMS, 'IntellectMoney', False);
        return true;
    }

    public function getImStatusFromOrder($orderId) {
        return $this->getParamFromOrder($orderId, 'imStatus');
    }

    public function getinvoiceIdFromOrder($orderId) {
        return $this->getParamFromOrder($orderId, 'invoiceId');
    }

    private function getParamFromOrder($orderId, $paramName) {
        $params = $this->getCustomDataFromOrder($orderId);
        return isset($params['custom_field'][$paramName]) ? $params['custom_field'][$paramName] : NULL;
    }

    public function saveImStatusToOrder($orderId, $imStatus) {
        return $this->saveParamToOrder($orderId, 'imStatus', $imStatus);
    }

    public function saveInvoiceIdToOrder($orderId, $invoiceId) {
        return $this->saveParamToOrder($orderId, 'invoiceId', $invoiceId);
    }

    private function saveParamToOrder($orderId, $paramName, $value) {
        $params = $this->getCustomDataFromOrder($orderId);
        $params['custom_field'][$paramName] = $value;
        $this->db->query("UPDATE `" . DB_PREFIX . "order` SET custom_field = '" . $this->db->escape(json_encode($params['custom_field'])) . "', date_modified = NOW() WHERE order_id = '" . (int) $orderId . "'");
    }

    private function getCustomDataFromOrder($orderId) {
        $this->load->model('checkout/order');
        $orderData = $this->model_checkout_order->getOrder($orderId);
        return isset($orderData['custom_field']) ? $orderData : false;
    }

}
