#Модуль оплаты платежной системы IntellectMoney для CMS OpenCart

> **Внимание!** <br>
Данная версия актуальна на *25 марта 2020 года* и не обновляется. <br>
Актуальную версию можно найти по ссылке https://wiki.intellectmoney.ru/display/TECH/Opencart#OpenCartFiles.

Инструкция по настройке доступна по ссылке https://wiki.intellectmoney.ru/display/TECH/Opencart#277545037288f5f1d2f843a391795f0ad74dd7ca
<br>
Ответы на частые вопросы можно найти здесь: https://wiki.intellectmoney.ru/display/TECH/Opencart#2775450363495f8bca704d02b0590ce4974dc084
